let firstName = "Andrei"
let secondName = "Kvasha"
let myGrowth = 185.1
let myWeight = 74
let myAge: UInt = 21
let iOS = "iOS Dev"
let myHobby = """
бегом, волейболом, изучаю английский и
немецкий (в декабре планирую сдать языковой сертификат по немецкому на уровень С1).
"""


let aboutMe = """
Всем привет!
Меня зовут \(firstName) \(secondName), мне \(myAge) год, мой рост \(myGrowth) см., вес \(myWeight) кг.
Учусь на 4 курсе по направлению 'Управление в технических системах'.
В мире \(iOS) совсем недолго, но данное направление захватывает меня сильнее, чем что-либо.
Параллельно занимаюсь \(myHobby)
"""

print(aboutMe)
